# Example\InfrastructureApiSymfonyBundle\Api\ProductApiInterface

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addProduct**](ProductApiInterface.md#addProduct) | **POST** /product | Add a new product
[**deleteProduct**](ProductApiInterface.md#deleteProduct) | **DELETE** /product/{productId} | Deletes a product
[**getProductById**](ProductApiInterface.md#getProductById) | **GET** /product/{productId} | Find product by ID
[**updateProduct**](ProductApiInterface.md#updateProduct) | **PUT** /product/{productId} | Update an existing product


## Service Declaration
```yaml
# config/services.yml
services:
    # ...
    Acme\MyBundle\Api\ProductApi:
        tags:
            - { name: "example_infrastructure_api_symfony.api", api: "product" }
    # ...
```

## **addProduct**
> Example\InfrastructureApiSymfonyBundle\Dto\Product addProduct($product)

Add a new product

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ProductApiInterface.php

namespace Acme\MyBundle\Api;

use Example\InfrastructureApiSymfonyBundle\Api\ProductApiInterface;

class ProductApi implements ProductApiInterface
{

    // ...

    /**
     * Implementation of ProductApiInterface#addProduct
     */
    public function addProduct(Product $product, int &$responseCode, array &$responseHeaders): array|object|null
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product** | [**Example\InfrastructureApiSymfonyBundle\Dto\Product**](../Model/Product.md)| Product object that needs to be created |

### Return type

[**Example\InfrastructureApiSymfonyBundle\Dto\Product**](../Model/Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **deleteProduct**
> deleteProduct($productId)

Deletes a product

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ProductApiInterface.php

namespace Acme\MyBundle\Api;

use Example\InfrastructureApiSymfonyBundle\Api\ProductApiInterface;

class ProductApi implements ProductApiInterface
{

    // ...

    /**
     * Implementation of ProductApiInterface#deleteProduct
     */
    public function deleteProduct(int $productId, int &$responseCode, array &$responseHeaders): void
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **int**| Product id to delete |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getProductById**
> Example\InfrastructureApiSymfonyBundle\Dto\Product getProductById($productId)

Find product by ID

Returns a single product

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ProductApiInterface.php

namespace Acme\MyBundle\Api;

use Example\InfrastructureApiSymfonyBundle\Api\ProductApiInterface;

class ProductApi implements ProductApiInterface
{

    // ...

    /**
     * Implementation of ProductApiInterface#getProductById
     */
    public function getProductById(int $productId, int &$responseCode, array &$responseHeaders): array|object|null
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **int**| ID of product to return |

### Return type

[**Example\InfrastructureApiSymfonyBundle\Dto\Product**](../Model/Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **updateProduct**
> Example\InfrastructureApiSymfonyBundle\Dto\Product updateProduct($productId, $product)

Update an existing product

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/ProductApiInterface.php

namespace Acme\MyBundle\Api;

use Example\InfrastructureApiSymfonyBundle\Api\ProductApiInterface;

class ProductApi implements ProductApiInterface
{

    // ...

    /**
     * Implementation of ProductApiInterface#updateProduct
     */
    public function updateProduct(int $productId, Product $product, int &$responseCode, array &$responseHeaders): array|object|null
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **int**| ID of product to return |
 **product** | [**Example\InfrastructureApiSymfonyBundle\Dto\Product**](../Model/Product.md)|  |

### Return type

[**Example\InfrastructureApiSymfonyBundle\Dto\Product**](../Model/Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

