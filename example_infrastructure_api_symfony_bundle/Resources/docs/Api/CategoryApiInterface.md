# Example\InfrastructureApiSymfonyBundle\Api\CategoryApiInterface

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addCategory**](CategoryApiInterface.md#addCategory) | **POST** /category | Add a new category
[**deleteCategory**](CategoryApiInterface.md#deleteCategory) | **DELETE** /category/{categoryId} | Deletes a category
[**getCategoryById**](CategoryApiInterface.md#getCategoryById) | **GET** /category/{categoryId} | Find category by ID
[**updateCategory**](CategoryApiInterface.md#updateCategory) | **PUT** /category/{categoryId} | Update an existing category


## Service Declaration
```yaml
# config/services.yml
services:
    # ...
    Acme\MyBundle\Api\CategoryApi:
        tags:
            - { name: "example_infrastructure_api_symfony.api", api: "category" }
    # ...
```

## **addCategory**
> Example\InfrastructureApiSymfonyBundle\Dto\Category addCategory($category)

Add a new category

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/CategoryApiInterface.php

namespace Acme\MyBundle\Api;

use Example\InfrastructureApiSymfonyBundle\Api\CategoryApiInterface;

class CategoryApi implements CategoryApiInterface
{

    // ...

    /**
     * Implementation of CategoryApiInterface#addCategory
     */
    public function addCategory(Category $category, int &$responseCode, array &$responseHeaders): array|object|null
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category** | [**Example\InfrastructureApiSymfonyBundle\Dto\Category**](../Model/Category.md)| Category object that needs to be created |

### Return type

[**Example\InfrastructureApiSymfonyBundle\Dto\Category**](../Model/Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **deleteCategory**
> deleteCategory($categoryId)

Deletes a category

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/CategoryApiInterface.php

namespace Acme\MyBundle\Api;

use Example\InfrastructureApiSymfonyBundle\Api\CategoryApiInterface;

class CategoryApi implements CategoryApiInterface
{

    // ...

    /**
     * Implementation of CategoryApiInterface#deleteCategory
     */
    public function deleteCategory(int $categoryId, int &$responseCode, array &$responseHeaders): void
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryId** | **int**| Category id to delete |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getCategoryById**
> Example\InfrastructureApiSymfonyBundle\Dto\Category getCategoryById($categoryId)

Find category by ID

Returns a single category

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/CategoryApiInterface.php

namespace Acme\MyBundle\Api;

use Example\InfrastructureApiSymfonyBundle\Api\CategoryApiInterface;

class CategoryApi implements CategoryApiInterface
{

    // ...

    /**
     * Implementation of CategoryApiInterface#getCategoryById
     */
    public function getCategoryById(int $categoryId, int &$responseCode, array &$responseHeaders): array|object|null
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryId** | **int**| ID of category to return |

### Return type

[**Example\InfrastructureApiSymfonyBundle\Dto\Category**](../Model/Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **updateCategory**
> Example\InfrastructureApiSymfonyBundle\Dto\Category updateCategory($categoryId, $category)

Update an existing category

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/CategoryApiInterface.php

namespace Acme\MyBundle\Api;

use Example\InfrastructureApiSymfonyBundle\Api\CategoryApiInterface;

class CategoryApi implements CategoryApiInterface
{

    // ...

    /**
     * Implementation of CategoryApiInterface#updateCategory
     */
    public function updateCategory(int $categoryId, Category $category, int &$responseCode, array &$responseHeaders): array|object|null
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categoryId** | **int**| ID of category to return |
 **category** | [**Example\InfrastructureApiSymfonyBundle\Dto\Category**](../Model/Category.md)|  |

### Return type

[**Example\InfrastructureApiSymfonyBundle\Dto\Category**](../Model/Category.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

