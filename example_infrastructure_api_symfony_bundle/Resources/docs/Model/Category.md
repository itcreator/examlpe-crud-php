# Category

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**parentId** | **int** |  | [optional] 
**name** | **string** |  | 
**subcategories** | [**Example\InfrastructureApiSymfonyBundle\Dto\Category**](Category.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


