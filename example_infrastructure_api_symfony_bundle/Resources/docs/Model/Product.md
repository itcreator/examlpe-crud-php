# Product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**categoryId** | **int** |  | 
**name** | **string** |  | 
**description** | **string** |  | [optional] 
**price** | **int** |  | [optional] 
**additionalAttributes** | [**Example\InfrastructureApiSymfonyBundle\Dto\AdditionalAttribute**](AdditionalAttribute.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


