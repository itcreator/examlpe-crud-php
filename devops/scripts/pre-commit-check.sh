#!/usr/bin/env bash

set -ex

HOST_UID=$(id -u) HOST_GID=$(id -g) docker compose run --rm -w /code/example_domain server vendor/bin/php-cs-fixer fix -v
HOST_UID=$(id -u) HOST_GID=$(id -g) docker compose run --rm -w /code/example_domain server vendor/bin/phpstan analyze -v
HOST_UID=$(id -u) HOST_GID=$(id -g) docker compose run --rm -w /code/example_domain server vendor/bin/phpunit

HOST_UID=$(id -u) HOST_GID=$(id -g) docker compose run --rm -w /code/example_app_symfony server vendor/bin/php-cs-fixer fix -v
HOST_UID=$(id -u) HOST_GID=$(id -g) docker compose run --rm -w /code/example_app_symfony server vendor/bin/phpstan analyze -v
