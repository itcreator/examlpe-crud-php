#!/usr/bin/env bash

set -ex

pwd

mkdir -p example_domain/var/cache

ln -sf ../../devops/scripts/pre-commit-check.sh .git/hooks/pre-commit

HOST_UID=$(id -u) HOST_GID=$(id -g) docker compose build --pull

HOST_UID=$(id -u) HOST_GID=$(id -g) docker compose run --rm -w /code/example_domain server composer install

HOST_UID=$(id -u) HOST_GID=$(id -g) docker compose run --rm -w /code/example_app_symfony server composer install
