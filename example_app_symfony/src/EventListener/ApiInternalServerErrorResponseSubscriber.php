<?php

namespace Example\AppSymfony\EventListener;

use Example\Domain\Repository\Exception\ProductAlreadyExists;
use Example\Domain\Type\Exception\WrongIdException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Generated controllers catch all exceptions and returns 500 error.
 * Wee need to catch this response and return response `Bad Request(400)` for recoverable errors.
 * Looks not clean but it works with OpenAPI Generator.
 */
class ApiInternalServerErrorResponseSubscriber implements EventSubscriberInterface
{
    final public function onKernelResponse(ResponseEvent $event): void
    {
        $response = $event->getResponse();
        $content = $response->getContent();

        if (Response::HTTP_INTERNAL_SERVER_ERROR !== $response->getStatusCode() || false === $content) {
            return;
        }

        try {
            $content = json_decode($content, true, 512, \JSON_THROW_ON_ERROR);
        } catch (\Throwable $e) {
            // TODO: log unserializable data

            return;
        }

        if (!is_array($content)) {
            // TODO: log serialization error
            return;
        }

        $this->handleErrorResponse($content, $event);
    }

    final public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => ['onKernelResponse', 0],
        ];
    }

    /* @phpstan-ignore-next-line */
    private function handleErrorResponse(array $content, ResponseEvent $event): void
    {
        $content = array_merge(['previous' => ['message' => null, 'type' => null]], $content);

        if (in_array($content['previous']['type'], [
            WrongIdException::class,
            ProductAlreadyExists::class,
        ], true)) {
            $response = new JsonResponse(
                [
                    'code' => 400,
                    'message' => $content['previous']['message'],
                ],
                Response::HTTP_BAD_REQUEST,
            );

            $response->headers->set('Content-Type', 'application/problem+json');
            $event->setResponse($response);
        }
    }
}
