<?php

namespace Example\AppSymfony\Api;

use Example\Domain\Entity\Category as CategoryEntity;
use Example\Domain\IdGeneratorInterface;
use Example\Domain\Type\Exception\WrongIdException;
use Example\Domain\Type\Id;
use Example\Domain\UseCase\CreateCategoryUC;
use Example\Domain\UseCase\ReadCategoryUC;
use Example\InfrastructureApiSymfonyBundle\Api\CategoryApiInterface;
use Example\InfrastructureApiSymfonyBundle\Dto\Category;

class CategoryApi implements CategoryApiInterface
{
    public function __construct(
        private readonly CreateCategoryUC $createCategoryUC,
        private readonly ReadCategoryUC $readCategoryUC,
        private readonly IdGeneratorInterface $idGenerator,
    ) {
    }

    /**
     * @throws WrongIdException
     * @phpstan-ignore-next-line
     */
    public function addCategory(Category $category, int &$responseCode, array &$responseHeaders): array|object|null
    {
        $parentId = null === $category->getParentId() ? null : new Id($category->getParentId());

        // TODO: use custom Mustache templates for OAS generator
        // name is required field, and it is ensured with Symfony\Component\Validator\Constraints\NotNull
        $categoryEntity = $this->createCategoryUC
            // @phpstan-ignore-next-line
            ->execute($this->idGenerator->generate(), $category->getName(), $parentId);

        return $this->categoryToDto($categoryEntity);
    }

    /**
     * @phpstan-ignore-next-line
     */
    public function deleteCategory(int $categoryId, int &$responseCode, array &$responseHeaders): void
    {
        // TODO: Implement deleteCategory() method.
    }

    /**
     * @phpstan-ignore-next-line
     *
     * @throws WrongIdException
     */
    public function getCategoryById(int $categoryId, int &$responseCode, array &$responseHeaders): array|object|null
    {
        $category = $this->readCategoryUC->execute(new Id($categoryId));

        if (null === $category) {
            $responseCode = 404;

            $result = [
                'message' => 'Category does not exists.',
            ];
        } else {
            $result = $this->categoryToDto($category);
        }

        return $result;
    }

    /**
     * @phpstan-ignore-next-line
     */
    public function updateCategory(int $categoryId, Category $category, int &$responseCode, array &$responseHeaders): array|object|null
    {
        // TODO: Implement updateCategory() method.
        return null;
    }

    public function categoryToDto(CategoryEntity $category): Category
    {
        $subcategories = [];

        foreach ($category->getSubcategories() as $subcategory) {
            $subcategories[] = $this->categoryToDto($subcategory);
        }

        return new Category([
            'id' => $category->id->value,
            'parentId' => $category->parentId instanceof Id ? $category->parentId->value : null,
            'name' => $category->name,
            'subcategories' => $subcategories,
        ]);
    }
}
