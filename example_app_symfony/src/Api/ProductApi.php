<?php

namespace Example\AppSymfony\Api;

use Example\Domain\Entity\Product as ProductEntity;
use Example\Domain\IdGeneratorInterface;
use Example\Domain\Repository\Exception\ProductAlreadyExists;
use Example\Domain\Type\Exception\WrongIdException;
use Example\Domain\Type\Id;
use Example\Domain\UseCase\CreateProductUC;
use Example\Domain\UseCase\DeleteProductUC;
use Example\Domain\UseCase\ReadProductUC;
use Example\Domain\UseCase\UpdateProductUC;
use Example\InfrastructureApiSymfonyBundle\Api\ProductApiInterface;
use Example\InfrastructureApiSymfonyBundle\Dto\Product;

class ProductApi implements ProductApiInterface
{
    public function __construct(
        private readonly CreateProductUC $createProductUC,
        private readonly ReadProductUC $readProductUC,
        private readonly UpdateProductUC $updateProductUC,
        private readonly DeleteProductUC $deleteProductUC,
        private readonly IdGeneratorInterface $idGenerator,
    ) {
    }

    /**
     * @throws ProductAlreadyExists
     * @throws WrongIdException
     * @phpstan-ignore-next-line
     */
    public function addProduct(Product $product, int &$responseCode, array &$responseHeaders): array|object|null
    {
        // name and category_id are required fields, and it is ensured with Symfony\Component\Validator\Constraints\NotNull
        $productEntity = $this->createProductUC
            // @phpstan-ignore-next-line
            ->execute($this->idGenerator->generate(), new Id($product->getCategoryId()), $product->getName(), $product->getDescription(), $product->getPrice());

        return $this->productToDto($productEntity);
    }

    /**
     * @throws WrongIdException
     * @phpstan-ignore-next-line
     */
    public function deleteProduct(int $productId, int &$responseCode, array &$responseHeaders): void
    {
        $this->deleteProductUC->execute(new Id($productId));
    }

    /**
     * @throws WrongIdException
     * @phpstan-ignore-next-line
     */
    public function getProductById(int $productId, int &$responseCode, array &$responseHeaders): array|object|null
    {
        $product = $this->readProductUC->execute(new Id($productId));

        if (null === $product) {
            $responseCode = 404;

            $result = [
                'message' => 'Product does not exists.',
            ];
        } else {
            $result = $this->productToDto($product);
        }

        return $result;
    }

    /**
     * @throws WrongIdException
     * @phpstan-ignore-next-line
     */
    public function updateProduct(int $productId, Product $product, int &$responseCode, array &$responseHeaders): array|object|null
    {
        // name and category_id are required fields, and it is ensured with Symfony\Component\Validator\Constraints\NotNull

        $productEntity = $this->updateProductUC->execute(
            new Id($productId),
            new Id($product->getCategoryId()),// @phpstan-ignore-line
            $product->getName(), // @phpstan-ignore-line
            $product->getDescription(),
            $product->getPrice(),
        );

        if (null === $productEntity) {
            $responseCode = 404;

            $result = [
                'message' => 'Product does not exists.',
            ];
        } else {
            $result = $this->productToDto($productEntity);
        }

        return $result;
    }

    public function productToDto(ProductEntity $product): Product
    {
        return new Product([
            'id' => $product->id->value,
            'categoryId' => $product->getCategoryId()->value,
            'name' => $product->getName(),
            'description' => $product->getDescription(),
            'price' => $product->getPrice(),
        ]);
    }
}
