<?php

namespace Example\DomainTest\Unit\Entity;

use Example\Domain\Entity\Product;
use Example\Domain\Type\Exception\WrongIdException;
use Example\Domain\Type\Id;

use function PHPUnit\Framework\assertEquals;

use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    /**
     * @throws WrongIdException
     */
    final public function testConstructor(): void
    {
        $product = new Product(new Id(1), new Id(2), 'TestProduct');

        assertEquals(1, $product->id->value);
        assertEquals(2, $product->getCategoryId()->value);
        assertEquals('TestProduct', $product->getName());
    }
}
