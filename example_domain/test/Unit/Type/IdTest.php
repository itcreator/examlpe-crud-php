<?php

namespace Example\DomainTest\Unit\Type;

use Example\Domain\Type\Exception\WrongIdException;
use Example\Domain\Type\Id;

use function PHPUnit\Framework\assertEquals;

use PHPUnit\Framework\TestCase;

class IdTest extends TestCase
{
    /**
     * @throws WrongIdException
     */
    final public function testConstructor(): void
    {
        $id = new Id(73);

        assertEquals(73, $id->value);
        assertEquals('73', (string) $id);
    }

    /**
     * @throws WrongIdException
     */
    final public function testInvalidId(): void
    {
        $this->expectException(WrongIdException::class);
        $id = new Id(0);
    }
}
