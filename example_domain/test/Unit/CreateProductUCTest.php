<?php

namespace Example\DomainTest\Unit;

use Example\Domain\EventDispatcher;
use Example\Domain\Mock\Repository\ProductRepositoryMock;
use Example\Domain\Repository\Exception\ProductAlreadyExists;
use Example\Domain\Type\Exception\WrongIdException;
use Example\Domain\Type\Id;
use Example\Domain\UseCase\CreateProductUC;

use function PHPUnit\Framework\assertEquals;

use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;

class CreateProductUCTest extends TestCase
{
    /**
     * @throws WrongIdException
     * @throws ProductAlreadyExists
     */
    final public function testCreateProduct(): void
    {
        $uc = new CreateProductUC(
            new ProductRepositoryMock(),
            new NullLogger(),
            new EventDispatcher(),
        );

        $product = $uc->execute(new Id(10), new Id(11), 'some name', 'some descr', 123);

        assertEquals(10, $product->id->value);
        assertEquals(11, $product->getCategoryId()->value);
        assertEquals('some name', $product->getName());
        assertEquals('some descr', $product->getDescription());
        assertEquals(123, $product->getPrice());
    }

    /**
     * @throws WrongIdException
     * @throws ProductAlreadyExists
     */
    final public function testProductCanNotBeCreatedTwice(): void
    {
        $uc = new CreateProductUC(
            new ProductRepositoryMock(),
            new NullLogger(),
            new EventDispatcher(),
        );

        $this->expectException(ProductAlreadyExists::class);

        $uc->execute(new Id(10), new Id(2), 'some name', 'some descr', 123);
        $uc->execute(new Id(10), new Id(2), 'some name');
    }
}
