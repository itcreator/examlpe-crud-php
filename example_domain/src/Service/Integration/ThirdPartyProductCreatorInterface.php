<?php

namespace Example\Domain\Service\Integration;

use Example\Domain\Type\Id;

interface ThirdPartyProductCreatorInterface
{
    public function create(Id $productId): void;
}
