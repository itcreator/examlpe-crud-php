<?php

namespace Example\Domain\Type;

use Example\Domain\Type\Exception\WrongIdException;

class Id
{
    /**
     * @throws WrongIdException
     */
    public function __construct(public readonly int $value)
    {
        if ($this->value < 1) {
            throw new WrongIdException($this->value);
        }
    }

    public function __toString(): string
    {
        return (string) $this->value;
    }
}
