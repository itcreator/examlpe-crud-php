<?php

namespace Example\Domain\Type\Exception;

use Example\Domain\DomainException;

class WrongIdException extends DomainException
{
    public function __construct(int $id)
    {
        parent::__construct("Wrong Id value {$id}");
    }
}
