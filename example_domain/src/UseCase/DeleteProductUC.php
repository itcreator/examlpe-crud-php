<?php

namespace Example\Domain\UseCase;

use Example\Domain\Repository\ProductRepositoryInterface;
use Example\Domain\Type\Id;
use Psr\Log\LoggerInterface;

class DeleteProductUC
{
    public function __construct(
        private readonly ProductRepositoryInterface $productRepository,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function execute(Id $productId): void
    {
        $this->productRepository->delete($productId);

        $this->logger->info("Product deleted Id: {$productId}");
    }
}
