<?php

namespace Example\Domain\UseCase;

use Example\Domain\Entity\Category;
use Example\Domain\Repository\CategoryRepositoryInterface;
use Example\Domain\Type\Id;
use Psr\Log\LoggerInterface;

class CreateCategoryUC
{
    public function __construct(
        private readonly CategoryRepositoryInterface $categoryRepository,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function execute(Id $categoryId, string $name, Id $parentId = null): Category
    {
        $category = new Category($categoryId, $name, $categoryId);

        $this->categoryRepository->create($category);

        $this->logger->info("Category created successfully Id: {$categoryId}, parentId: {$parentId}, name: {$name}");

        return $category;
    }
}
