<?php

namespace Example\Domain\UseCase;

use Example\Domain\Entity\Product;
use Example\Domain\Repository\ProductRepositoryInterface;
use Example\Domain\Type\Id;
use Psr\Log\LoggerInterface;

class ReadProductUC
{
    public function __construct(
        private readonly ProductRepositoryInterface $productRepository,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function execute(Id $productId): ?Product
    {
        $product = $this->productRepository->get($productId);

        $this->logger->info("Read product. Id: {$productId}");

        return $product;
    }
}
