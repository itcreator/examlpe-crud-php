<?php

namespace Example\Domain\UseCase;

use Example\Domain\Entity\Category;
use Example\Domain\Repository\CategoryRepositoryInterface;
use Example\Domain\Type\Id;
use Psr\Log\LoggerInterface;

class ReadCategoryUC
{
    public function __construct(
        private readonly CategoryRepositoryInterface $categoryRepository,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function execute(Id $categoryId): ?Category
    {
        $category = $this->categoryRepository->get($categoryId);

        $this->logger->info("Read category. Id: {$categoryId}");

        return $category;
    }
}
