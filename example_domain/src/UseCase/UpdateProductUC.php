<?php

namespace Example\Domain\UseCase;

use Example\Domain\Entity\Product;
use Example\Domain\Repository\ProductRepositoryInterface;
use Example\Domain\Type\Id;
use Psr\Log\LoggerInterface;

class UpdateProductUC
{
    public function __construct(
        private readonly ProductRepositoryInterface $productRepository,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function execute(Id $productId, Id $categoryId, string $name, ?string $description, ?int $price): ?Product
    {
        $product = $this->productRepository->get($productId);

        if (null === $product) {
            return null;
        }

        $product->assignToCategory($categoryId);
        $product->changeName($name);
        $product->changeDescription($description);
        $product->changePrice($price);

        $this->productRepository->update($product);

        $this->logger->info("Product updated successfully Id: {$productId}, catId: {$categoryId}, name: {$name}");

        return $product;
    }
}
