<?php

namespace Example\Domain\UseCase;

use Example\Domain\Entity\Product;
use Example\Domain\Event\ProductCreatedEvent;
use Example\Domain\EventDispatcher;
use Example\Domain\Repository\Exception\ProductAlreadyExists;
use Example\Domain\Repository\ProductRepositoryInterface;
use Example\Domain\Type\Id;
use Psr\Log\LoggerInterface;

class CreateProductUC
{
    public function __construct(
        private readonly ProductRepositoryInterface $productRepository,
        private readonly LoggerInterface $logger,
        private readonly EventDispatcher $eventDispatcher,
    ) {
    }

    /**
     * @throws ProductAlreadyExists
     */
    public function execute(Id $productId, Id $categoryId, string $name, ?string $description = null, ?int $price = null): Product
    {
        $product = new Product($productId, $categoryId, $name, $description, $price);

        $this->productRepository->create($product);

        $this->logger->info("Product created successfully Id: {$productId}, catId: {$categoryId}, name: {$name}");

        $this->eventDispatcher->emit(new ProductCreatedEvent($productId));

        return $product;
    }
}
