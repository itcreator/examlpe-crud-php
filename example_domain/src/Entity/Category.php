<?php

namespace Example\Domain\Entity;

use Example\Domain\Type\Id;

class Category
{
    /**
     * Note: For real project would be better to implement generic Tree class such as Tree<T> {T item, array<T>}
     *      When T is Category
     *      But we have time limit and will select the most simple way with data denormalization in domain layer.
     *
     * @var array<int, Category> Denormalized field
     */
    private array $subcategories = [];

    /**
     * Note: In Doctrine repository we will add fields which need to implement Materialized Path pattern
     *      But it is just details.
     */
    public function __construct(
        public readonly Id $id,
        public readonly string $name,
        public readonly ?Id $parentId = null,
    ) {
    }

    /**
     * @return array<int, Category>
     */
    public function getSubcategories(): array
    {
        return $this->subcategories;
    }

    public function appendSubcategory(Category $category): void
    {
        // TODO: check duplicates. But better will be to implement generic Tree class
        $this->subcategories[] = $category;
    }
}
