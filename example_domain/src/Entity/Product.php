<?php

namespace Example\Domain\Entity;

use Example\Domain\Type\Id;

class Product
{
    public function __construct(
        public readonly Id $id,
        // One-To-Many unidirectional
        private Id $categoryId,
        private string $name,
        private ?string $description = null,
        private ?int $price = null,
    ) {
    }

    public function getCategoryId(): Id
    {
        return $this->categoryId;
    }

    public function assignToCategory(Id $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function changeName(string $name): void
    {
        $this->name = $name;
    }

    public function changeDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function changePrice(?int $price): void
    {
        $this->price = $price;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }
}
