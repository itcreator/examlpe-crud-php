<?php

namespace Example\Domain;

use Example\Domain\Event\EventInterface;
use Example\Domain\Listener\ListenerInterface;

class EventDispatcher
{
    /**
     * @param array<ListenerInterface> $listeners
     */
    public function __construct(
        private readonly array $listeners = [],
    ) {
    }

    public function emit(EventInterface $event): void
    {
        foreach ($this->listeners as $listener) {
            $listener->probe($event);
        }
    }
}
