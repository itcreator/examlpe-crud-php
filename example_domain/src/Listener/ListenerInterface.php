<?php

namespace Example\Domain\Listener;

use Example\Domain\Event\EventInterface;

interface ListenerInterface
{
    public function probe(EventInterface $event): void;
}
