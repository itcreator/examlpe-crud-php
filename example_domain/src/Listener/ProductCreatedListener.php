<?php

namespace Example\Domain\Listener;

use Example\Domain\Event\EventInterface;
use Example\Domain\Event\ProductCreatedEvent;
use Example\Domain\Service\Integration\ThirdPartyProductCreatorInterface;
use Psr\Log\LoggerInterface;

class ProductCreatedListener implements ListenerInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ThirdPartyProductCreatorInterface $creator,
    ) {
    }

    public function probe(EventInterface $event): void
    {
        if (!($event instanceof ProductCreatedEvent)) {
            return;
        }

        $this->logger->debug(sprintf('Handling event "%s"', ProductCreatedEvent::class));

        $this->creator->create($event->productId);
    }
}
