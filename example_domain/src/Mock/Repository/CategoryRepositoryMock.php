<?php

namespace Example\Domain\Mock\Repository;

use Example\Domain\Entity\Category;
use Example\Domain\Repository\CategoryRepositoryInterface;
use Example\Domain\Type\Exception\WrongIdException;
use Example\Domain\Type\Id;

class CategoryRepositoryMock implements CategoryRepositoryInterface
{
    /**
     * @var array<int, array{"item": Category, "children": array<int, Id>}>
     */
    private array $categories = [];

    /**
     * @throws WrongIdException
     */
    public function __construct()
    {
        // Data for tests

        $this->create(new Category(new Id(1), 'root'));
        $this->create(new Category(new Id(2), 'cat #1', new Id(1)));
        $this->create(new Category(new Id(3), 'cat #2', new Id(1)));
        $this->create(new Category(new Id(4), 'cat #1.1', new Id(2)));
        $this->create(new Category(new Id(5), 'cat #1.1', new Id(2)));
    }

    public function create(Category $category): void
    {
        // TODO: throw an exception if duplicated

        $this->categories[$category->id->value] = [
            'item' => $category,
            'children' => [],
        ];

        if (null !== $category->parentId) {
            $parentId = $category->parentId->value;
            // TODO: throw an exception if parent not found
            // TODO: throw an exception if tree is cyclic
            $this->categories[$parentId]['children'][] = $category->id;
        }
    }

    public function get(Id $categoryId): ?Category
    {
        if (!isset($this->categories[$categoryId->value])) {
            return null;
        }

        $node = $this->categories[$categoryId->value];

        // TODO: check max depth

        /** @var Category $category */
        $category = $node['item'];

        foreach ($node['children'] as $childId) {
            $subcategory = $this->get($childId);
            if ($subcategory instanceof Category) {
                $category->appendSubcategory($subcategory);
            }
            // TODO: else: data broken. throw an exception
        }

        return $category;
    }

    public function update(Category $category): void
    {
        // TODO: Implement update() method.
    }

    public function delete(Id $categoryId): void
    {
        // TODO: Implement delete() method.
    }
}
