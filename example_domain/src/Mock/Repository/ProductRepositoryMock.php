<?php

namespace Example\Domain\Mock\Repository;

use Example\Domain\Entity\Product;
use Example\Domain\Repository\Exception\ProductAlreadyExists;
use Example\Domain\Repository\ProductRepositoryInterface;
use Example\Domain\Type\Exception\WrongIdException;
use Example\Domain\Type\Id;

class ProductRepositoryMock implements ProductRepositoryInterface
{
    /** @var array<string|int, Product> */
    private array $products = [];

    /**
     * @throws WrongIdException
     */
    public function __construct()
    {
        $this->products = [
            '1' => new Product(new Id(1), new Id(1), 'Product #1.1', 'Descr', 20),
            '2' => new Product(new Id(2), new Id(1), 'Product #1.2'),
            '3' => new Product(new Id(3), new Id(1), 'Product #1.3'),
            '4' => new Product(new Id(4), new Id(1), 'Product #1.4'),

            '5' => new Product(new Id(5), new Id(2), 'Product #2.1'),
            '6' => new Product(new Id(6), new Id(2), 'Product #2.2'),
            '7' => new Product(new Id(7), new Id(2), 'Product #2.3'),
            '8' => new Product(new Id(8), new Id(2), 'Product #2.4'),
        ];
    }

    public function create(Product $product): void
    {
        $key = (string) $product->id;
        if (isset($this->products[$key])) {
            throw new ProductAlreadyExists($product->id);
        }

        $this->products[$key] = $product;
    }

    public function get(Id $productId): ?Product
    {
        return $this->products[(string) $productId] ?? null;
    }

    public function update(Product $product): void
    {
        $this->products[(string) $product->id] = $product;
    }

    public function delete(Id $productId): void
    {
        $key = (string) $productId;
        if (isset($this->products[$key])) {
            unset($this->products[$key]);
        }
    }
}
