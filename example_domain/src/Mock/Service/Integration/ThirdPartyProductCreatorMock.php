<?php

namespace Example\Domain\Mock\Service\Integration;

use Example\Domain\Entity\Product;
use Example\Domain\Repository\ProductRepositoryInterface;
use Example\Domain\Service\Integration\ThirdPartyProductCreatorInterface;
use Example\Domain\Type\Id;
use Psr\Log\LoggerInterface;

class ThirdPartyProductCreatorMock implements ThirdPartyProductCreatorInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ProductRepositoryInterface $productRepository,
    ) {
    }

    public function create(Id $productId): void
    {
        $product = $this->productRepository->get($productId);

        if ($product instanceof Product) {
            $this->logger->info("Product created in 3rd party. Id: {$productId}, Name: {$product->getName()} ");
        }
        // TODO: else: throw an exception. Data broken.
    }
}
