<?php

namespace Example\Domain\Mock;

use Example\Domain\IdGeneratorInterface;
use Example\Domain\Type\Exception\WrongIdException;
use Example\Domain\Type\Id;

class IdGeneratorMock implements IdGeneratorInterface
{
    private int $current = 8;

    /**
     * @throws WrongIdException
     */
    public function generate(): Id
    {
        ++$this->current;

        return new Id($this->current);
    }
}
