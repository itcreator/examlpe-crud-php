<?php

namespace Example\Domain\Repository\Exception;

use Example\Domain\DomainException;
use Example\Domain\Type\Id;

class ProductAlreadyExists extends DomainException
{
    public function __construct(Id $id)
    {
        parent::__construct("Product with ID {$id} already exists");
    }
}
