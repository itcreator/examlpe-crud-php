<?php

namespace Example\Domain\Repository;

use Example\Domain\Entity\Category;
use Example\Domain\Type\Id;

/**
 * For some application will be better to segregate this interface
 * But it is not critical for this example
 */
interface CategoryRepositoryInterface
{
    public function create(Category $category): void;

    public function get(Id $categoryId): ?Category;

    public function update(Category $category): void;

    public function delete(Id $categoryId): void;
}
