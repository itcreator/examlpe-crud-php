<?php

namespace Example\Domain\Repository;

use Example\Domain\Entity\Product;
use Example\Domain\Repository\Exception\ProductAlreadyExists;
use Example\Domain\Type\Id;

/**
 * For some application will be better to segregate this interface
 * But it is not critical for this example
 */
interface ProductRepositoryInterface
{
    /**
     * @throws ProductAlreadyExists
     */
    public function create(Product $product): void;

    public function get(Id $productId): ?Product;

    public function update(Product $product): void;

    public function delete(Id $productId): void;
}
