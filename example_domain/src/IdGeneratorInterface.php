<?php

namespace Example\Domain;

use Example\Domain\Type\Id;

interface IdGeneratorInterface
{
    public function generate(): Id;
}
