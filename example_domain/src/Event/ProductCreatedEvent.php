<?php

namespace Example\Domain\Event;

use Example\Domain\Type\Id;

class ProductCreatedEvent implements EventInterface
{
    public function __construct(
        public readonly Id $productId,
    ) {
    }
}
